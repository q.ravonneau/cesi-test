# Extensions VSCode

Prettier

# Titre

## Sous-titre

Autre titre
===========

\# echappement
`# pas interprété`

Saut de ligne => 2 espaces ou 2 sauts de ligne

*Italique*  
**Gras**  
_Italique_  
__Gras__  

## Liste à puces
Liste de tâche
--------------
- Première tâche
    - Première sous-tâche
- Deuxième tâche
    - Première sous-tâche
    - Deuxième sous-tâche

## Liste numérotée
1. Première tâche
    1. Première sous-tâche
2. Deuxième tâche
    1. Première sous-tâche
    2. Deuxième sous-tâche

## Liste combinée

1. étape 1
2. étape 2
    - Sous-étape

```Javascript
const maFonction = () => {
    console.log("Hello world !");
}
```

```html
<div>
    <h1>Mon titre</h1>
    <p>
        Paragraphe
    </p>
</div>
```

## Citation
> Ceci est une citation

Lien vers le site du [CESI](http://www.cesi.fr)

Lien vers le site du [CESI][site du cesi]

[site du cesi]

![logo CESI](https://www.cesi.fr/wp-content/uploads/2022/07/lg_cesi.png)


|Article | Quantité |Prix|
|---------|:----:|-------:|
|Fruits|3|10.5|
|Légumes|4|9|

## Lignes
--------------
**************
- - - - - - - 

## Notes de bas de page
Text  
[^1] Note de bas de page


## Note GIT
git rm --cached README.md  
->Supprimer le fichier de l'index

Working Dir > Index > Tree

git log --graph

git config --global user.email "mail@mail"

comment changer nom de la branche par defaut ?

git restore

git commit -a
-> status + commit

git commit --amend
-> modification du commentaire du commit

git add . && git commit --amend -m "modif commentaire"

## Exercice 1

- initialiser un projet git
- créer un fichier README.md
- ajouter du text
- ajouter à l'index
- réaliser un commit
- vérifier l'index et l'historique
- réaliser une autre modif
- ajouter à l'index
- réaliser un deuxième commit
- modifier le fichier
- modifier le dernier commit
- vérifier l'historique
- comparer le SHA du commit

commit 993d3ced0cbaf5e40fa7633a1647ad458e01624c
commit 03624e84bd4aaf700d7c7a0610f1978aae28b76d

modif à effacer

git checkout
-> permet de naviguer entre les commit

git switch -
-> revenir à la branche HEAD

git rebase --i
-> mode interactif
https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase

CONTENU A SUPPRIMER

squash
drop
squash
pick


## Branches

je suis sur la develop

merge <branchName> no-ff


## Exercice FF
fast-forward et no-fast-forward

- creer une branche des feature1 depuis develop
- faire 2 commit
- creer feature2 depuis develop
- faire 2 commits
- merge feature2 dans develop
- rebase feature1 depuis develop
- merge feature1 dans develop

## Ressources
- https://docs.github.com/fr/get-started/using-git/about-git-rebase
- https://jwiegley.github.io/git-from-the-bottom-up/1-Repository/8-interactive-rebasing.html
- https://learngitbranching.js.org/?locale=fr_FR

git branch --set-upstream-to <repo>/<branch>
pick
drop
squash
pick
je suis sur la feature
git checkout -b <branchName>

git merge <branchAMerge>

git branch -d <branchName>

git branch <branchName> #créer une branche
git switch <branchName> #comme checkout mais valable juste pour switch

merge <branchName> no-ff

feature1
feature2
commit1
commit2

# Gitlab
## Création et configuration d'un projet

## Exercice merge request
- aligner main et develop
- Faire 1 commit sur develop
- Créer branche de release
- Créer merge request branche de release vers main
- Valider merge request
- Toutes les branches doivent être alignées